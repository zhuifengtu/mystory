# mystory

## Build Setup

```bash
# install dependencies
$ yarn install

# synchronize data from git submodule (for this project only) 
$ git submodule update --remote --recursive --init

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
